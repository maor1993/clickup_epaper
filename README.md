# clickup_epaper

Toolset to connect Clickup to a waveshare epaper display using python


## How to Use

Create your own clickupauth.py at epaperstasks/clickup with this class:

```
class MyAuth:
    auth_token = "YOUR TOKEN"
    this_year_id = "THIS YEAR ID"
    long_term_id = "LONG TERM ID"
    space_id = "YOUR SPACE ID"
```


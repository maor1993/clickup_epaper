import unittest
from pathlib import Path
from html2image import Html2Image
import random




class TestHtmlToImage(unittest.TestCase):
    def testBasicSanity(self):
        hti = Html2Image(output_path='tmp',custom_flags=['--no-sandbox'])

        tmpDirPath= Path('tmp')
        tmpDirPath.mkdir(parents=True,exist_ok=True)



        hti.screenshot(
            html_file='mockup/index.html',
            css_file=['mockup/bootstrap.min.css','mockup/style.css'],
            save_as='out.png',
            size=(384,640)
        )

        self.assertTrue(Path('tmp/out.png').is_file())

if __name__ == "__main__":
    unittest.main()
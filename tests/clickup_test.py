import unittest
import epapertasks.clickup.clickup as cu


mockServerPath= "https://private-anon-bf4e629f85-clickup20.apiary-mock.com/api/v2/list/"

class demoauth:
    auth_token = "fakeauth"
    this_year_id = "555"
    long_term_id = "123"




class TestClickupAPI(unittest.TestCase):
    @unittest.skip("Skipping clickup tests due to bug in mockup server")
    def testThisYearAPI(self):
        self.assertNotEqual(cu.get_this_year_task_names(demoauth,mockServerPath),[],"")
    @unittest.skip("Skipping clickup tests due to bug in mockup server")        
    def testLongTermAPI(self):
        self.assertNotEqual(cu.get_long_term_task_names(demoauth,mockServerPath),[],"")        

if __name__ == '__main__':
    unittest.main()
import unittest
import epapertasks.htmlgen.htmlgeneator as htmlgen





class TestHtmlGenerator(unittest.TestCase):

    def testBasicResult(self):
        myPage = htmlgen.HtmlPage(5)

        myPage.addTask({"name":"Note1"})


        longPos = myPage.getHtmlFile().find("id=\"longTerm\"")
        thisPos = myPage.getHtmlFile().find("id=\"thisYear\"")

        found = myPage.getHtmlFile().find("Note1")



        self.assertNotEqual(found,-1)  
        self.assertFalse(thisPos<=found<=longPos) 
    def testListMode(self):
        jeffList = [{"name":"jeff1"},{"name":"jeff2"},{"name":"jeff3"}]


        myPage = htmlgen.HtmlPage(5)

        myPage.addTask(jeffList,"")
        
        for jeff in jeffList:   
            found = myPage.getHtmlFile().find(jeff['name'])
            self.assertNotEqual(found,-1)  



if __name__ == '__main__':
    unittest.main()

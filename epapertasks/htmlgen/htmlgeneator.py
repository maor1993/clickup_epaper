# Keep it simple, stupid!
htmlHeader = """
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=480, height=640, initial-scale=1.0">
    <title>Document</title>
    <!-- CSS only -->
    <link rel="stylesheet" href="./bootstrap.min.css" >
    <link rel="stylesheet" href="./style.css">
</head>
"""

htmlBody = """
<body>
    <h1 class="text-center">Maors Dreams</h1>
    <div id="longTerm">
        <h2 class="text-center">Long Term</h2>
        <div class="container ">
            <div class="row justify-content-center">
                FIXMELONGTERM
            </div>
        </div>
    </div>
    <div id="thisYear">
        <h2 class="text-center">This Year</h2>
        <div class="container ">
            <div class="row justify-content-center">
                FIXMETHISYEAR
            </div>
        </div>
    </div>
    <div id="Soon">
    <h2 class="text-center">Soon</h2>
    <ul>
        FIXMESOON
    </ul>  
</div>
</body>
</html>
"""

htmlExclemationPoint=  """<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-lg" viewBox="0 0 16 16">
                <path d="M6.002 14a2 2 0 1 1 4 0 2 2 0 0 1-4 0zm.195-12.01a1.81 1.81 0 1 1 3.602 0l-.701 7.015a1.105 1.105 0 0 1-2.2 0l-.7-7.015z"/>
              </svg>"""


class  HtmlPage:
    def __init__(self,maxNotes):
        self.maxNotes = maxNotes
        self.longStr = ""
        self.thisStr = ""
        self.soonStr = ""
        self.htmlBody = htmlBody
        pass
    def clearNotes(self):
        self.longStr = ""
        self.thisStr = ""
        self.soonStr = ""
    def addTask(self,text,path="long"):
        if type(text) is not list: textList = [text]
        else: textList = text

        for item in textList:
            if path=="long": #FIXME:support max count of notes
                self.longStr+= "<div class=\"col-md-1 post-it\">"+item['name']+"</div>\n\r"
            elif path=="soon":
                urgentStr =""
                if item['urgent']:
                    urgentStr=htmlExclemationPoint
                self.soonStr+= "<li>"+item['name']+urgentStr+"</li>"
            else:
                self.thisStr+= "<div class=\"col-md-1 post-it\">"+item['name']+"</div>\n\r"
        return 
    def getHtmlFile(self):
        htmlFull = htmlHeader
        
        # create a copy of the htmlbody we're about to use.
        htmlBodyNew = self.htmlBody
    
        htmlBodyNew = htmlBodyNew.replace("FIXMELONGTERM",self.longStr)  
        htmlBodyNew = htmlBodyNew.replace("FIXMETHISYEAR",self.thisStr)
        htmlBodyNew = htmlBodyNew.replace("FIXMESOON",self.soonStr)
        htmlFull+= htmlBodyNew

        return htmlFull





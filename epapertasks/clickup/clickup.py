# clickup simple api
# desciption: uses the clickup 2.0 api to get a spesific list from 


# imports
import requests
from datetime import datetime




def get_task_names(listId,authtoken,apipath='https://api.clickup.com/api/v2/list/'):
    # perfrom api call to get the list.
    headers = {
    'Authorization': authtoken,
    'Content-Type': 'application/json'
    }
    request = requests.get(apipath+listId+'/task', headers=headers)
    # if call is okay, flatten in the json to get the task names
    if request.status_code == 200:
        task_names = []
        task_json = request.json()
        for item in task_json['tasks']:
            task_names.append({"name":item['name']})
        return task_names
    elif request.status_code == 429: # ratelimit issue
        print("Rate Limit occured")
    return []    

'''
get_list_tasks
returns all the tasks info from a list
'''
def get_list_tasks(listId,authtoken,apipath='https://api.clickup.com/api/v2/'):
    # perfrom api call to get the list.
    headers = {
    'Authorization': authtoken,
    'Content-Type': 'application/json'
    }
    request = requests.get(apipath+'/list/'+listId+'/task', headers=headers)
    # if call is okay, flatten in the json to get the task names
    if request.status_code == 200:
        task_json = request.json()
        return task_json['tasks']
    elif request.status_code == 429: # ratelimit issue
        print("Rate Limit occured")
    return []    

'''
get_Space_lists
Returns a dictornary of list names and list Ids
'''
def get_space_lists(workspaceId,authtoken,apipath='https://api.clickup.com/api/v2'):
    headers = {
        'Authorization': authtoken
    }
    request = requests.get(apipath+'/space/'+str(workspaceId)+'/folder', headers=headers)
    # if call is okay, flatten in the json to get the task names
    if request.status_code == 200:
        list_list = []
        folder_json = request.json()
        #walk into each folder and extract the lists
        for folder in folder_json['folders']:
            for list in folder['lists']:
                list_list.append({"id":list["id"],"name":list["name"]})
        return list_list
    return []





def get_this_year_task_names(authobject,apipath='https://api.clickup.com/api/v2/list/'):
    return get_task_names(authobject.this_year_id,authobject.auth_token,apipath=apipath)


def get_long_term_task_names(authobject,apipath='https://api.clickup.com/api/v2/list/'):
    return get_task_names(authobject.long_term_id,authobject.auth_token,apipath=apipath)

def get_all_workspace_lists(authobject,apipath='https://api.clickup.com/api/v2'):
    return get_space_lists(authobject.space_id,authobject.auth_token)

def get_all_workspace_tasks(authobject,apipath='https://api.clickup.com/api/v2'):
    list_list = get_space_lists(authobject.space_id,authobject.auth_token)
    all_tasks =[]
    for myList in list_list:
        all_tasks += get_list_tasks(myList['id'],authobject.auth_token)
    return all_tasks

def get_urgent_tasks(authobject,apipath='https://api.clickup.com/api/v2'):
    list_list = get_space_lists(authobject.space_id,authobject.auth_token)
    all_tasks =[]
    for myList in list_list:
        all_tasks += get_list_tasks(myList['id'],authobject.auth_token)

    return list(filter(lambda x:x['priority']!=None and x['priority']['priority']=="urgent",all_tasks))


def get_urgent_task_names(authobject,apipath='https://api.clickup.com/api/v2'):
    myUrgentTasks = get_urgent_tasks(authobject)

    urgentTaskNames = []
    for task in myUrgentTasks:
        overDue = False
        if task['due_date'] is not None:
            overDue = datetime.today()>datetime.fromtimestamp(int(task['due_date'][:-3]))
        urgentTaskNames.append({'name':task['name'],'urgent':overDue})
    return urgentTaskNames

if __name__ == "__main__":
    from epapertasks.clickup.clickupauth import MyAuth # internal file with your access token and private api paths
    # print(get_this_year_task_names(MyAuth))
    # print(get_long_term_task_names(MyAuth))
    # print(get_all_workspace_lists(MyAuth))
    print(get_urgent_task_names(MyAuth))


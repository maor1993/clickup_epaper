# EpaperTasks




# imports

# local
import htmlgen.htmlgeneator as htgen
import clickup.clickup as cu
from clickup.clickupauth import MyAuth


# from pip
from html2image import Html2Image
from PIL import Image


# builtin
import argparse
from time import sleep
from pathlib import Path
import shutil
import io
from distutils.dir_util import copy_tree


# constants
VERSION_MAJOR = 0
VERSION_MINOR = 1
sleepTime = 60*60 #FIXME: this needs to come from arguments



# fucntions
def generateAndSaveHtmlImage(page:htgen.HtmlPage,thisYearTasks,longTermTasks,soonTasks):
    page.addTask(thisYearTasks,path="year")
    page.addTask(longTermTasks,path="long")
    page.addTask(soonTasks,path="soon")

    
    hti = Html2Image(output_path='tmp')

    with open('tmp/index.html','w') as fd:
        fd.write(page.getHtmlFile())
    hti.screenshot(
        html_file='tmp/index.html',
        css_file=['tmp/bootstrap.min.css','tmp/style.css'],
        save_as='out.png',
        size=(384,640)
    )


def is_raspberrypi():
    try:
        with io.open('/sys/firmware/devicetree/base/model', 'r') as m:
            if 'raspberry pi' in m.read().lower(): return True
    except Exception: pass
    return False



# main
if __name__ == "__main__":
    isRpi = is_raspberrypi()

    print("Clickup Tasks to Epaper Version "+ str(VERSION_MAJOR)+"."+str(VERSION_MINOR))

    if isRpi:
        print("Running on Raspberry Pi")
        import epaper.epd7in5bc as epd
        disp = epd.EPD()
         

    print("Step 0: setting up env...")
    myPage = htgen.HtmlPage(5)
    tmpDirPath= Path('tmp')
    tmpDirPath.mkdir(parents=True,exist_ok=True)
    copy_tree("./assets","./tmp")
    print("Step 1: Trying to contact your clickup Tasks...")


    print("Step 2: starting up epaper display")

    prevhtmlstr = ""
    while(True):
        myPage.clearNotes()
        print("step 3: getting your clickup Tasks")
        try:
            thisYearTasks = cu.get_this_year_task_names(MyAuth)
            longTermTasks = cu.get_long_term_task_names(MyAuth)
            soonTasks = cu.get_urgent_task_names(MyAuth)

            print("step 4: generating html and converting to image")
            generateAndSaveHtmlImage(myPage,thisYearTasks,longTermTasks,soonTasks)
        except:
            print(f"Failed to get New Image from clickup, trying again")
            continue

        print("step 5: sending to Epaper...")
        htmlImage = Image.open("tmp/out.png")
        
        if isRpi:
            if prevhtmlstr != myPage.getHtmlFile():
                disp.init()
                disp.Clear()
                disp.display(disp.getbuffer(htmlImage))
            else:
                print("Skipping send as image html has not changed.")
        prevhtmlstr = myPage.getHtmlFile()

        print("Done! sleeping for "+ str(sleepTime) + " Seconds")
        
        sleep(sleepTime)


